import express, { Application, Request, Response } from 'express';
import socket, { Socket } from 'socket.io';
import cors, { CorsOptions } from 'cors';

// cors setup
const corsOptions: CorsOptions = {
  origin: '*',
  optionsSuccessStatus: 200,
};

// server setup
const app: Application = express();

const server = app.listen(4000, () => {
  console.log('server started on port 4000');
});

// test endpoint
app.get('/card/:id', cors(corsOptions), ({ params: { id } }: Request, res: Response) => {
  const card = {
    id,
    text: 'hui pirozok',
    type: 'white',
  };

  res.send(card);
});


// socket setup
const io = socket(server);

io.on('connection', (socket: Socket) => {
  console.log(`made web socket connection, socket id: ${socket.id}`);
});
